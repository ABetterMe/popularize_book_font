const http = uni.$u.http

export const getIndexSwiper = (data) => http.get('/data/api.data/getSlider', {
	params: data
})

export const getCode = (data) => http.get('/data/api.data/getCode', {
	params: data
})

export const getPlatform = () => http.get('/data/api.mission/getPlatform')

export const getRule = (data) => http.get('/data/api.mission/getRules', {
	params: data
})

export const getRanks = () => http.get('/data/api.data/getRank')

export const getNotify = (data) => http.get('/data/api.data/getNotify', {
	params: data
})

export const getUpgrade = () => http.get('/data/api.data/getUpgrade')

export const postSubContact = (data) => http.post('/data/api.data/subContact', data)

export const getCourseCate = () => http.get('/data/api.data/getCourseCate')

export const getBookCategorys = () => http.get('/data/api.data/getBookCategorys')

export const getMissions = (data) => http.get('/data/api.mission/getMissions', {
	params: data
})

export const getAuthMissions = (data) => http.get('/data/api.auth.mission/getMissions', {
	params: data
})

export const getCourses = (data) => http.get('/data/api.Course/getList', {
	params: data
})

export const getCourseItems = (data) => http.get('/data/api.Course/getCourseItems', {
	params: data
})

export const getMissionDetail = (data) => http.get('/data/api.mission/getMissionDetail', {
	params: data
})

export const postLogin = (data) => http.post('/data/api.wxapp/session', data)

export const getOpenIdByCode = (data) => http.post('/data/api.wxapp/getOpenId', data)
export const loginByPhone = (data) => http.post('/data/api.wxapp/loginByPhone', data)

export const postRegister = (data) => http.post('/data/api.wxapp/decode', data)

export const phoneRegOrLogin = (data) => http.post('/data/api.wxapp/phoneLoginOrReg', data)

export const reLogin = function(s) {
	return new Promise((resolve, reject) => {
		uni.login({
			provider: 'weixin',
			success: function(loginRes) {
				http.post('/data/api.wxapp/session', {
					code: loginRes.authCode
				}).then(ret => {
					uni.setStorageSync('UserInfo', ret || {});
					uni.setStorageSync('UserToken', ret.token.token || '');
					resolve(ret);
				}).catch((err) => {
					reject(err)
				});
			}
		});
	});
}

export const getRegion = () => http.get('/data/api.data/getRegion')

export const postUserInfo = (data) => http.post('/data/api.auth.center/set', data)

export const getPopuPlatforms = () => http.get('/data/api.data/getPopuPlatforms')

export const getCategores = (data) => http.get('/data/api.data/getCategores', {
	params: data
})

export const getSearchData = () => http.get('/data/api.data/getSearchData')

export const getSearchData1 = () => http.get('/data/api.data/getSearchData1')

export const getAccountList = (data) => http.get('/data/api.auth.center/getAccountList', {
	params: data
})

export const getAccount = (data) => http.get('/data/api.auth.center/getAccount', {
	params: data
})

export const postAccount = (data) => http.post('/data/api.auth.center/accountSave', data)

export const accountDel = (data) => http.get('/data/api.auth.center/accountDel', {
	params: data
})

export const postKeyWords = (data) => http.post('/data/api.auth.mission/addKeywords', data)

export const posJointMission = (data) => http.post('/data/api.auth.mission/joinMission', data)

export const getKeyWords1 = (data) => http.get('/data/api.auth.mission/getKeywords', {
	params: data
})

export const delKeyWords = (data) => http.post('/data/api.auth.mission/delKeyword', data)

export const getFeedBackList = (data) => http.get('/data/api.auth.mission/getFeedBackList', {
	params: data
})

export const receiveKeywords = (data) => http.post('/data/api.auth.mission/receiveKeywords', data)

export const getKeyword = (data) => http.get('/data/api.auth.mission/getKeyword', {
	params: data
})

export const getPostAccount = (data) => http.get('/data/api.auth.mission/getPostAccount', {
	params: data
})

export const postKeyWord = (data) => http.post('/data/api.auth.mission/editKeyword', data)

export const postFeedBack = (data) => http.post('/data/api.auth.mission/addFeedBack', data)


export const getAboutUs = (data) => http.get('/data/api.data/aboutUs', {
	params: data
})

export const getOrders = (data) => http.get('/data/api.auth.order/getList', {
	params: data
})

export const getIncomeList = (data) => http.get('/data/api.auth.order/getIncomeList', {
	params: data
})

export const getOrderCollect = () => http.get('/data/api.auth.order/getOrderCollect')

export const getKeywordOrderCollect = (data) => http.get('/data/api.auth.order/getKeywordOrderCollect', {
	params: data
})

export const getKeywordOrder = (data) => http.get('/data/api.auth.order/getKeywordOrder', {
	params: data
})

export const getOtherOrderCollect = () => http.get('/data/api.auth.order/getOtherOrderCollect')

export const getCashAccount = () => http.get('/data/api.auth.center/cashAccount')

export const getBanks = () => http.get('/data/api.data/getBanks')

export const getCashAccountDetail = (data) => http.get('/data/api.auth.center/getCashAccount', {
	params: data
})

export const postPayAccount = (data) => http.post('/data/api.auth.center/setCashAccount', data)

export const applyTransfer = (data) => http.post('/data/api.auth.transfer/add', data)
export const getTransfers = (data) => http.get('/data/api.auth.transfer/get', {
	params: data
})

export const getFrom = (data) => http.get('/data/api.auth.center/getFrom', {
	params: data
})

export const getQrcode = () => http.get('/data/api.auth.center/qrcode')

export const getUserImage = () => http.get('/data/api.auth.center/getUserImage')

export const upgradeUser = () => http.get('/data/api.auth.center/upgradeUser')

export const getPaymentChannel = () => http.get('/data/api.auth.order/channel')

export const getPayment = (data) => http.post('/data/api.auth.course/payment', data)

export const getOrder = (data) => http.get('/data/api.auth.course/getOrder', {
	params: data
})

export const addPlay = (data) => http.get('/data/api.course/play', {
	params: data
})

export const getComments = (data) => http.get('/data/api.course/getComment', {
	params: data
})

export const addComment = (data) => http.post('/data/api.auth.course/addComment', data)

export const getFromOne = (data) => http.get('/data/api.auth.center/getFromOne', {
	params: data
})

export const getFromOrder = (data) => http.get('/data/api.auth.order/getFromOrder', {
	params: data
})

export const getConfig = () => http.get('/data/api.data/getConfig')
export const getConfig1 = () => http.get('/data/api.data/getConfig1')
export const getOtherConfig = () => http.get('/data/api.auth.center/getMoreConfig')

// export const getFromOne = (data) => http.get('/data/api.data/getFromOne', {
// 	params: data
// })

// export const getFromOrder = (data) => http.get('/data/api.data/getFromOrder', {
// 	params: data
// })
