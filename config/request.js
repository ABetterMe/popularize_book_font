module.exports = (vm) => {
	// 初始化请求配置
	uni.$u.http.setConfig((config) => {
		config.baseURL = 'https://dtkj.diankuai.com/';
		return config
	})

	// 请求拦截
	uni.$u.http.interceptors.request.use((config) => { // 可使用async await 做异步操作

		config.data = config.data || {}

		// #ifdef H5-MOBILE
		config.header['Api-Name'] = 'wap';
		// #endif

		// #ifdef MP-WEIXIN
		config.header['Api-Name'] = 'wxapp';
		// #endif

		// #ifdef H5-WEIXIN
		config.header['Api-Name'] = 'wechat';
		// #endif

		var UserToen = uni.getStorageSync('UserToken');
		// 接口认证及请求方式
		config.header['Api-Token'] = UserToen ? UserToen : "-";
		config.header['Content-Type'] = 'application/x-www-form-urlencoded';
		return config
	}, config => { // 可使用async await 做异步操作
		return Promise.reject(config)
	})

	// 响应拦截
	uni.$u.http.interceptors.response.use((response) => {
		/* 对响应成功做点什么 可使用async await 做异步操作*/
		const data = response.data

		// 自定义参数
		const custom = response.config?.custom
		if (response.statusCode !== 200) {
			// 如果没有显式定义custom的toast参数为false的话，默认对报错进行toast弹出提示
			if (custom.toast !== false) {
				uni.$u.toast(data.info)
			}

			// 如果需要catch返回，则进行reject
			if (custom?.catch) {
				return Promise.reject(data)
			} else {
				// 否则返回一个pending中的promise，请求不会进入catch中
				return new Promise(() => {})
			}
		} else if (response.statusCode === 200 && data.code !== 1) {
			if (data.code === 401) {
				uni.login({
					provider: 'weixin',
					success: function(loginRes) {
						uni.$u.http.post('/data/api.wxapp/session', {
							code: loginRes.code
						}).then(ret => {
							uni.setStorageSync('UserInfo', ret || {});
							uni.setStorageSync('UserToken', ret.token.token || '');
						}).catch((err) => {
							
							uni.removeStorageSync('UserInfo');
							uni.removeStorageSync('UserToken');

							var refreshed = uni.getStorageSync('refreshed', 0);
							
							if (refreshed == 0) {
								const pages = getCurrentPages()
								const curPage = pages[pages.length - 1]
								curPage.onLoad(curPage.options) // 传入参数
								curPage.onShow()
								curPage.onReady()
								uni.setStorageSync('refreshed', 1);
							}

						});
					}
				});
				return Promise.reject(data)
			} else if (data.code == 0) {
				return Promise.reject(data)
			}
		}
		return data.data === undefined ? {} : data.data
	}, (response) => {
		// 对响应错误做点什么 （statusCode !== 200）
		return Promise.reject(response)
	})
}
