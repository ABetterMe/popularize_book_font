export const cal_page_height = function(className) {
	return new Promise((resolve, reject) => {
		uni.getSystemInfo({
			success: resu => {
				const query = uni.createSelectorQuery();
				query.select(className).boundingClientRect();
				query.exec(res => {
					var height = resu.windowHeight - res[0].height - resu
						.statusBarHeight + 'px';
					resolve(height)
				});
			},
			fail: res => {
				reject(res)
			}
		});
	});
}
